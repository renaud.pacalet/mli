\input{hdr}

\DeclareMathOperator{\band}{and}
\DeclareMathOperator{\bor}{or}
\DeclareMathOperator{\argmax}{argmax}
\DeclareMathOperator{\avg}{avg}
\DeclareMathOperator{\bigo}{\mathcal{O}}
\DeclareMathOperator{\DES}{DES}
\DeclareMathOperator{\DESX}{DES-X}
\DeclareMathOperator{\bgcd}{gcd}

\setbeamercolor{postit}{fg=black,bg=blue!10!white}

\newcommand{\postit}[2]{\tiny\begin{beamercolorbox}[wd=#1,sep=0pt,center,shadow=true,rounded=true]{postit}#2\end{beamercolorbox}}
\newcommand{\mybox}[2]{\postit{#1}{#2}}
\newcommand{\source}[2]{\mybox{#1}{Source: #2}}
\newcommand{\qitem}{\item[?]}
\newcommand{\aitem}{\item[\checkmark]}

\newcommand{\theTitle}{Timing attacks... and what is special with DES SBox \#5?}
\newcommand{\theAuthors}{R.~Pacalet}
\newcommand{\theConferenceAndPlace}{}
\newcommand{\theDate}{\today}

\begin{document}

\makeFirstFrame

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Auguste Kerckhoffs' principles (1883)}
  \begin{itemize}
    \item The system must be substantially, if not mathematically, undecipherable;
    \item {\em The system must not require secrecy and can be stolen by the enemy without
      causing trouble;}
    \item It must be easy to communicate and remember the keys without requiring written
      notes, it must also be easy to change or modify the keys with different participants;
    \item The system ought to be compatible with telegraph communication;
    \item The system must be portable, and its use must not require more than one person;
    \item Finally, regarding the circumstances in which such system is applied, it must be
      easy to use and must neither require stress of mind nor the knowledge of a long
      series of rules.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The spirit is strong, the flesh is weak}
  \begin{itemize}
    \item Eventually, security is always implemented in hardware
    \item An Intel Core i7 processor is a piece of hardware
    \item A smartcard too
    \item Your brain, a pencil, a piece of paper too
    \item Hardware computes and does also some more...
      \begin{itemize}
        \item Consumes power
        \item Takes time
        \item Emits electromagnetic radiations
        \item Plus heat, noise,...
      \end{itemize}
    \item While apparently not exploitable these ''side channels'' are serious information leaks
  \end{itemize}
\end{frame}

\section{Side channels}

\begin{frame}
  \frametitle{Hardware leaks information}
  \begin{itemize}
    \item The ''side-channels'' are usually correlated with the processing
    \item In security applications they can be used to retrieve embedded secrets
    \item Less than 1000 power traces can be sufficient to retrieve a secret key from a theoretically unbreakable system
    \item Unlike in quantum cryptography the information leakage is usually undetectable
      \begin{itemize}
        \item True for time
        \item Almost true for power
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{A bit of history}
  \begin{itemize}
    \item 1956: MI5 against Egyptian Embassy in London (click-sound of the enciphering machine)
    \item 1996: P. Kocher time-attacks RSA, DH, DSS (applied with success in 2003 against OpenSSL 0.9.6)
    \item 1999: P. Kocher power-attacks DES, AES, etc. (SPA, DPA). Successful against smart cards, FPGAs, ...
    \item 2000-: New attacks (SEMA, DEMA, TPA, MIA,...).
    \item Importance of hardware security increases (CHES)
  \end{itemize}
\end{frame}

\section{DES}

\begin{frame}
  \frametitle{History (1/2)}
  \begin{itemize}
    \item Symmetric bloc cipher
    \item Initial proposal by IBM (Lucifer) in 1975
    \item Adopted by NSA in 1977 (after some modifications) under the name DES: Data Encryption Standard
    \item Feistel scheme, 56 bits key, 16 rounds
    \item Some analytical attacks (differential and linear cryptanalysis), impractical
    \item Brute force attacks became feasible
      \begin{itemize}
        \item $2^{56} \approx 7.2 \times 10^{16}$
        \item About one year on 1000 PCs @ 2GHz, with 800 clock cycles per DES enciphering (OpenSSL, Eric Young)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{History (2/2)}
  \begin{itemize}
    \item Triple DES recommended today: $TDEA_{k_1,k_2,k_3}(P)=\DES_{k_1}(\DES^{-1}_{k_2}(\DES_{k_3}(P)))$
    \item Some interesting variants: $\DESX_{k_1,k,k_2}(P)=k_1\oplus \DES_k(k_2\oplus P)$
    \item Superseded in 2002 by AES: Advanced Encryption Standard
    \item Still in use, probably for some time
      \begin{itemize}
        \item Easy to implement in hardware (small and fast)
        \item In software, AES is simpler to implement (no bitwise permutations)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithm (1/4)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES_state-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Algorithm (2/4)}
  \begin{figure}[htbp]
    \scalebox{0.7}{\input{build/DES_f-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Algorithm (2/4)}
  \begin{figure}[htbp]
    \scalebox{0.7}{\input{build/DES_f5-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Algorithm (3/4)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES_key-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Algorithm (4/4)}
  \begin{itemize}
    \item $IP$, $FP$, $P$ and $PC_1$ are pure permutation functions
    \item $E$ is an expansion ($32\Rightarrow48$) -- permutation function
    \item $PC_2$ is a selection ($56\Rightarrow48$) -- permutation function
    \item $S_1$, $S_2$, ..., $S_8$ are non-linear, 6 bits to 4 bits, functions: the substitution boxes or ''SBoxes''
    \item The key-schedule consists in simple or double left circular rotations (specified for each round)
    \item The decryption function is obtained by inverting the key-schedule (right circular rotations)
  \end{itemize}
\end{frame}

\section{Timing attacks, the HWSec lab case}

\begin{frame}
  \frametitle{History of timing attacks}
  \begin{itemize}
    \item First published by Paul Kocher (CRYPTO’96)
    \item Implemented by Dhem, Quisquater, et al.  (CARDIS’98)
    \item Used by Canvel, Hiltgen, Vaudenay, and Vuagnoux to attack OpenSSL (CRYPTO’03)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Principle of timing attacks (1/2)}
  \begin{itemize}
    \item Crypto function $E_K$ use secret key $K$ to compute output $C=E_K(P)$ from input $P$.
    \item Joint exploit of computing time $T$ and output $C$ (or input $P$, depending on what is known by the attacker)
    \item Attacher builds partial timing model $TM_g(C)$ depending on \textit{guessed} value $g$ for \textit{small} part $k$ of secret $K$
    \item $TM_g(C)$ is an estimate of time taken by an elementary operation of $E_K$ when computing $C=E_K(P)$... if guess $g$ is correct...
    \item ... else $TM_g(C)$ is just random
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Principle of timing attacks (2/2)}
  \begin{itemize}
    \item Run $C_i=E_K(P_i)$ on $N$ random inputs and record $\{C_i,T_i\},\ 1\le i\le N$ pairs of \{output, computing time\}
    \item For each possible value $g$ (guess) of $k$ compute the $TM_g(C_i),\ 1\le i\le N$...
    \item ...and estimate correlation $\mathcal{C}_g$ between $T_i$ and $TM_g(C_i)$ over the $N$ measurements
    \item One guess $g_{best}$ exhibits the best correlation $\mathcal{C}_{g_{best}}$ $\Rightarrow$ best guess for $k$ is $g_{best}$
    \item Repeat for all (\textit{small}) parts $k$ of $K$ $\Rightarrow$ best guess for $K$
    \item Same measurements collection can be used for all parts $k$ of $K$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example of TA-prone implementation (1/2)}
  \begin{itemize}
    \item DES software implementation, used in timing attack lab of HWSec course
    \item \url{http://soc.eurecom.fr/HWSec/labs/des\_ta.php}
    \item DES software implementation in plain C
    \item All DES functions based on lookup tables
    \item Data-independent timing (apart caches and branch prediction)...
    \item ... but the $P$ permutation...
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example of TA-prone implementation (2/2)}
  \lstinputlisting[language=C,caption=]{src/p.c}
\end{frame}

\begin{frame}
  \frametitle{The timing attack lab of HWSec}
  \begin{itemize}
    \item Students run the TA-prone DES on arbitrary large number $N$ of random, unknown, inputs $P_{1\le i\le N}$
    \item They record the $\{C_i,T_i\}_{1\le i\le N}$ pairs (output, computing time)
    \item They do not know the secret key $K$
    \item They must design an attack to recover last round key $K_{16}$
    \item Validation: their attack is run with increasing number of measurements until success
    \item Their score is minimum number of measurements needed (including 100 consecutive positives)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Zoom on last round (known output)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_all-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Known and unknown data}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_1-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Let us focus on SBox \#1}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_2-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Let us focus on SBox \#1}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_3-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Simplify ($S_1, P$)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_4-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Let us guess $k_{16}^{(1)}$ (64 cases)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_5-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Let us compute the $S_1$ output ($P$ input)}
  \begin{figure}[htbp]
    \scalebox{0.45}{\input{build/DES16_6-fig.pdf_t}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \begin{columns}
    \column{3cm}
    \begin{figure}[htbp]
      \scalebox{0.45}{\input{build/DES16_6-fig.pdf_t}}
    \end{figure}
    \column{8cm}
    \begin{itemize}
      \item For each known output $C_i$ ($N$ of them)...
      \item for each guess ($64$ of them) on $k_{16}^{(1)}$...
      \item the attacker can compute 4 bits output of $S_1$ (input of permutation $P$)...
      \item and thus estimate computation time of $P$... if the guess is correct
      \item Example of (simple) timing model:
        \begin{itemize}
          \item $S_1$ output = \texttt{0b0000} $\Rightarrow$ $P$ fast
          \item $S_1$ output = \texttt{0b1111} $\Rightarrow$ $P$ slow
        \end{itemize}
      \item Same for all SBoxes ($8$ of them)...
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{The simple attack algorithm}
  \begin{algorithm}[H]
    \scriptsize
    \begin{algorithmic}[1]
      \For{$s \gets 1...8$} \Comment For all SBoxes
      \For{$g \gets 0...63$} \Comment For all guesses on $k_{16}^{(s)}$
          \State $t_{slow} \gets \varnothing$, $t_{fast} \gets \varnothing$ \Comment Initialize ''slow'' and ''fast'' sets
          \For{$i \gets 1...N$} \Comment For all measurements
            \State $v \gets S_s(g,C_i)$ \Comment Compute output of SBox $s$ in last round
            \If{$v = \texttt{0b0000}$} \Comment Four zero-bits
              \State $t_{fast} \gets t_{fast}\cup\{T_i\}$ \Comment Put timing measurement in ''fast'' set
            \ElsIf{$v = \texttt{0b1111}$} \Comment Four one-bits
              \State $t_{slow} \gets t_{slow}\cup\{T_i\}$ \Comment Put timing measurement in ''slow'' set
            \EndIf
          \EndFor
          \State $\mathcal{C}_g \gets \avg(t_{slow})-\avg(t_{fast})$ \Comment Score of guess $g$ for $k_{16}^{(s)}$
        \EndFor
        \State $k_{16}^{(s)}=\argmax_g(\mathcal{C}_g)$ \Comment Take best guess $g$ for $k_{16}^{(s)}$
      \EndFor
      \State \textbf{return} $K_{16}=k_{16}^{(1)}|k_{16}^{(2)}|...|k_{16}^{(8)}$ \Comment Best guess for last round key $K_{16}$
    \end{algorithmic}
  \end{algorithm}
\end{frame}

\begin{frame}
  \frametitle{Should it work?}
  \begin{itemize}
    \item The timing model predicts only a very small part of the total computation time
      \begin{itemize}
        \item Only in one round over 16...
        \item Only in the $P$ permutation...
        \item Only on 4 of the 32 input bits...
      \end{itemize}
    \item Everything else is either
      \begin{itemize}
        \item Uncorrelated (noise)
        \item Correlated
      \end{itemize}
    \item If uncorrelated, the averaging over a \textit{large enough} number of measurements shall eliminate the noise due to other computations (and
      measurements errors)...
    \item ... else the proposed attack could (should) fail
    \item By luck (for attackers), this statistical independence holds rather well in cryptography (confusion, diffusion)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Does it work?}
  \begin{itemize}
    \item Sometimes, sometimes not (about 50\%-50\%, depends on secret key $K$). Students are frustrated and complain...
    \item Last year I explained that keeping only $v=\texttt{0b0000}$ and $v=\texttt{0b1111}$ was discarding too many measurements ($7/8$), thus degrading the
      attack's efficiency
    \item This year I tried with 1 million measurements and it failed
    \item All 6-bits parts $k_{16}^{(s)}$ of $K_{16}$ were recovered with less than 5000 measurements... but $k_{16}^{(5)}$ (SBox \#5) that resisted with any
      number of measurements
    \item This year I decided to understand why
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{What is special with DES SBox \#5?}
  \begin{itemize}
    \item I designed statistical tools to check statistical independence
    \item Statistical independence does not fully hold, there are some singularities (known since linear and differential cryptanalysis)
    \item But there is more:
      \begin{itemize}
        \item There are four 6-bits inputs $x$ such that $S_5(x)=\texttt{0b0000}$ (normal, the DES SBoxes are $6\Rightarrow4$ bits and balanced):
          \begin{itemize}
            \item $X_0=\{\texttt{0b011010},\texttt{0b010011},\texttt{0b111100},\texttt{0b110101}\}$
          \end{itemize}
        \item There are four 6-bits inputs $x$ such that $S_5(x)=\texttt{0b1111}$:
          \begin{itemize}
            \item $X_1=\{\texttt{0b010110},\texttt{0b010101},\texttt{0b110000},\texttt{0b110011}\}$
          \end{itemize}
      \end{itemize}
    \item<2-> {\color{red}$X_0=X_0\oplus \texttt{0b100110}$\ !}
    \item<3-> {\color{red}$X_1=X_1\oplus \texttt{0b100110}$\ !!!}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{What is special with DES SBox \#5?}
  \begin{itemize}
    \item {\color{red}$X_0=X_1\oplus \texttt{0b100110}$}
    \item {\color{red}$X_1=X_1\oplus \texttt{0b100110}$}
    \item Two values of $k^{(5)}$ exhibit the same ''signature'' for simple attack
    \item If $g$ is a \textit{good guess} for $k^{(5)}$, $g'=g\oplus \texttt{0b100110}$ also is:
      \begin{itemize}
        \item $\forall\ 0\le r\le63,\ S_5(g\oplus r)=\texttt{0b0000}\Leftrightarrow S_5(g'\oplus r)=\texttt{0b0000}$
        \item $\forall\ 0\le r\le63,\ S_5(g\oplus r)=\texttt{0b1111}\Leftrightarrow S_5(g'\oplus r)=\texttt{0b1111}$
      \end{itemize}
    \item With statistical independence, the attack should indefinitely oscillate between $k^{(5)}$ and $k^{(5)}\oplus\texttt{0b100110}$
    \item Statistical independence does not really hold $\Rightarrow$ attack converges towards one or other, depending on actual $K$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusion, open questions}
  \begin{itemize}
    \item Could this $S_5$ property be exploited for new attacks?
    \item Could this kind of properties inspire design of new, naturally TA-resistant, ciphers?
    \item Is this accidental or shall we revisit DES history?
      \begin{itemize}
        \item The probability to draw such a ''à-la-DES'' SBox is $1/1100.5$
        \item The probability to draw at least one when designing DES is about $1/138$
      \end{itemize}
    \item Could it be that the other SBoxes also have hidden properties?
      \begin{itemize}
        \item Sylvain Guilley discovered one on $S_4$
        \item  $S_5$ is also known to be the less non-linear of all
      \end{itemize}
    \item How can it be that after more than 40 years of deep scrutiny we still discover something new about DES?
    \item Is it really new?
  \end{itemize}
\end{frame}

\end{document}
