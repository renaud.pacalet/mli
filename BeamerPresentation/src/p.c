// The table giving the input positions of each output bit. Output bit #1, for 
// instance, is input bit #16 and output bit #32 is input bit #25.
p_table = {16,  7, 20, 21,
           29, 12, 28, 17,
            1, 15, 23, 26,
            5, 18, 31, 10,
            2,  8, 24, 14,
           32, 27,  3,  9,
           19, 13, 30,  6,
           22, 11,  4, 25};

p_permutation(val) {
  res = 0;                       // Initialize the result to all zeros
  for(i = 1; i <= 32; i++) {     // For all 32 input bits...
    if(get_bit(val, i) == 1) {   // ... if input bit is set...
      for(j = 1; j <= 32; j++) { // ... for all 32 output positions...
        if(p_table[j] == i)      // ... if input position is input bit
          k = j;                 // Remember output position
      }                          // k now contains output position of input bit
      set_bit(res, k);           // Set bit #k of result
    }
  }
  return res;                    // Return result
}
